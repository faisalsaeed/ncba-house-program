

// Custom JS 

/*
* @author: Faisal Saeed
*/

$(document).ready(function () {

  $('.dropdown').on('show.bs.dropdown', function () {
    $(this).find('.dropdown-menu').slideDown();
  });
  // Add slideUp animation to Bootstrap dropdown when collapsing.
  $('.dropdown').on('hide.bs.dropdown', function () {
    $(this).find('.dropdown-menu').slideUp();
  });


  /*add class in header*/
  $(window).on("scroll", function () {
    if ($(window).scrollTop() >= 500) {
      $("header").addClass("header-scroll ");
    } else {
      $("header").removeClass("header-scroll ");
    }
  });


  /* avatar uploader */
  var readURL = function (input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('.profile-pic').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $(".file-upload").on('change', function () {
    readURL(this);
  });

  $(".upload-button").on('click', function () {
    $(".file-upload").click();
  });


  /*change theme mode toggle */
  $('#change-mode').change(function () {
    if ($(this).is(":checked")) {
      $('body').addClass("darkmode");
    } else {
      $('body').removeClass("darkmode");
    }
  });

 /*show input on button click */
  $(".edit-icon-name").click(function() {
    $(".name-input").show();
  });

   /*enable button ater vlaue in password field */
  $(':input[type="submit"]').prop('disabled', true);
  $('input[type="password"]').keyup(function () {
    if ($(this).val() != '') {
      $(':input[type="submit"]').prop('disabled', false);
    }
  });


  /*form validation*/
  (function () {
    'use strict'
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.classList.add('was-validated')
        }, false)
      })
  })()

  

});

var scene = document.getElementById('parallex');
var parallax = new Parallax(scene);


